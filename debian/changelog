prestwood (0.2024.2) apertis; urgency=medium

  * Drop debian/source/apertis-component, moved to debian/apertis/component
  * Fix syntax of debian/copyright
  * Drop autotools-dev from Build-Deps, not required anymore since debhelper 10

 -- Dylan Aïssi <daissi@debian.org>  Thu, 24 Aug 2023 17:29:38 +0200

prestwood (0.2024.1) apertis; urgency=medium

  * Update debian/apertis/copyright
  * Add missing license information to help
    license scan to generate a complete report.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 13 Jun 2023 10:20:23 +0530

prestwood (0.2024.0+apertis0) apertis; urgency=medium

  * Append apertis suffix

 -- Vignesh Raman <vignesh.raman@collabora.com>  Thu, 02 Mar 2023 07:35:35 +0530

prestwood (0.2024.0co1) apertis; urgency=medium

  * doc: Update documentation and build it with GTK-doc

 -- Pulluri Shirija <Pulluri.Shirija@in.bosch.com>  Fri, 21 Feb 2023 15:20:03 +0530

prestwood (0.2022.0+apertis1) apertis; urgency=medium

  * Add debian/apertis/lintian
  * Debian/control: fix packages description
  * Switch from deprecated priority extra to optional
  * Refresh the automatically detected licensing information
  * Override lintian no-manual-page tag
  * prestwood-dev: add dependency on gir1.2-prestwoodinterface-0
  * Switch to debhelper-compat (= 13)
  * Drop dh_install --fail-missing, it's the default since debhelper 13

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Mon, 23 Jan 2023 11:45:41 +0100

prestwood (0.2022.0co2) apertis; urgency=medium

  * Remove debian/apertis/gitlab-ci.yml
  * Add debian/apertis/copyright

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Fri, 18 Mar 2022 12:22:03 +0100

prestwood (0.2022.0co1) apertis; urgency=medium

  * hotdoc-0.8 dependency removed

 -- Akshay M <M.Akshay@in.bosch.com>  Mon, 28 Feb 2022 05:17:23 +0000

prestwood (0.2020.1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to target

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 01:31:40 +0000

prestwood (0.2020.1) apertis; urgency=medium

  * Switch to native format to work with the GitLab-to-OBS pipeline.
  * gitlab-ci: Link to the Apertis GitLab CI pipeline definition.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Mon, 20 Jan 2020 20:03:16 +0800

prestwood (0.1706.0co4) apertis; urgency=medium

  * Switch to native source package format.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Mon, 20 Jan 2020 20:01:42 +0800

prestwood (0.1706.0-0co4) apertis; urgency=medium

  * debian/control: Depend on the hotdoc-0.8 branch

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 08 Jan 2020 16:02:22 +0000

prestwood (0.1706.0-0co3) apertis; urgency=medium

  * AppArmor: Allow access to mount related files

 -- Frédéric Danis <frederic.danis@collabora.com>  Thu, 19 Dec 2019 10:18:10 +0100

prestwood (0.1706.0-0co2) 17.06; urgency=medium

  * AppArmor: Put prestwood in enforcing mode

 -- Simon McVittie <smcv@collabora.com>  Tue, 16 May 2017 19:05:47 +0100

prestwood (0.1706.0-0co1) 17.06; urgency=medium

  * Opportunistic AppArmor fixes (Apertis: T3915):
    - Allow logging to the Journal
    - Consolidate gvfs rules

 -- Simon McVittie <smcv@collabora.com>  Wed, 12 Apr 2017 15:23:01 +0100

prestwood (0.1703.1-0co1) 17.03; urgency=medium

  [ Frédéric Dalleau ]
  * Fix .arcconfig
  * debian: do not try to compile schemas at postinst

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Fri, 03 Mar 2017 08:55:34 +0000

prestwood (0.1703.0-0co1) 17.03; urgency=medium

  [ Mathieu Duponchelle ]
  * Require hotdoc version 0.8
    - docs/reference/Makefile.am: use --gdbus-codegen-sources

  [ Simon McVittie ]
  * Add debian/source/apertis-component marking this as a target package
  * Stop telling Canterbury about UPnP shares and removable devices
    (part of T3486)
  * Code and packaging cleanup
    - Do not depend on deprecated dbus-glib library, fixing FTBFS
    - Simplify startup and teardown
    - Move to debhelper compat level 10
    - Remove manually-maintained debug symbols package
    - Split out libprestwoodiface into its own binary package

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Wed, 25 Jan 2017 14:40:13 +0000

prestwood (0.2.4-0co1) 16.09; urgency=medium

  [ Nandini Raju ]
  * Added GLib message logging mechanism.

  [ Mathieu Duponchelle ]
  * Documentation: stop using gtk-doc
  * Remove TODO
  * debian: Remove doc-base file.
  * git.mk: update to upstream master
  * configure.ac: enable silent rules
  * documentation: port to hotdoc 0.8

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Thu, 15 Sep 2016 23:14:10 +0200

prestwood (0.2.3-0co1) 16.06; urgency=medium

  [ Abhiruchi Gupta ]
  * Prestwood: vim-style modeline comment added.

  [ Philip Withnall ]
  * debian: Add missing copyright header to AppArmor profile
  * debian: Add AppArmor rules for network, Frampton, Canterbury, GVFS
    (Apertis: T1807)

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Wed, 06 Jul 2016 20:00:17 +0100

prestwood (0.2.2-0co1) 16.03; urgency=medium

  [ Aleksander Morgado ]
  * interface: fix build of gdbus-codegen generated sources
  * interface: build gdbus-codegen generated sources early
  * interface: add introspection support
  * build: use AX_COMPILER_FLAGS
  * debian: introspection package renamed to match the .gir filename
  * build: rebuild gtk-doc sections always

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Tue, 01 Mar 2016 16:30:28 +0100

prestwood (0.2.1-0co1) 16.03; urgency=medium

  [ Philip Withnall ]
  * src: Ignore mount failures rather than assuming success (CHAI: #3434)
  * build: Add .arcconfig file

  [ Guillaume Desmottes ]
  * arcconfig: add 'default-reviewers' field
  * debian: fix install of AppArmor profile (Apertis: #617)

  [ Simon McVittie ]
  * debian/source/options: ignore git.mk and .arcconfig in the Debian
    diff, since these are not intended to appear in tarballs but may
    appear in git
  * Improve the AppArmor profile (Apertis: #617, T802):
    - add standard abstraction for Apertis components
    - no need to allow Prestwood to re-execute itself
    - allow Prestwood to own its own D-Bus name
    - remove permissions that seem to be copy/paste from canterbury
    - allow Canterbury to terminate Prestwood
  * Clean up build system to allow future continuous integration (T770):
    - Use a static README file and the standard Autotools INSTALL
    - Use usual name for systemduserunitdir, and put it in the ${prefix}
    - Tidy up cleaning rules
    - autogen.sh: don't set special CFLAGS, CXXFLAGS (T823)
    - Don't distribute files that are distributed implicitly
    - Use AM_INIT_AUTOMAKE([-Wno-portability]) instead of autogen.sh
    - Use the generic GNOME autogen.sh
    - Install the static libraries
    - Fail the build if anything except *.la is built but not installed
    - debian/rules: use autogen.sh so we can build directly from git
    - Remove undesired dh_shlibdeps override
    - debian/source/options: ChangeLog is a generated file, ignore it
    - debian/docs: don't explicitly install NEWS
    - build: remove unused PKGDATADIR and PROGRAMNAME_LOCALEDIR
    - build: use correct CPPFLAGS for out-of-tree build
  * Replace entry point schema with a desktop entry file (T573)

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Mon, 25 Jan 2016 10:55:56 +0000

prestwood (0.2.0-0rb2) 15.03; urgency=low

  *  Initial import

 --  Apertis package maintainers <packagers@lists.apertis.org>  Thu, 05 Feb 2015 17:03:53 +0530
