# Makefile to build all of the service code in the fi/ subdirectories. This uses
# non-recursive automake to allow the services to be built in parallel, and
# allow sharing of build rules in this file.

# To add a new service:
#  1. Add a section for it to this file, appending to lib_LTLIBRARIES and
#     includesfi_HEADERS.
#  2. Add it to the library list in docs/reference/Makefile.am.
#  3. Add its documentation to the includes in
#     docs/reference/canterbury-docs.xml.
#  4. Add it to the library list and CPPFLAGS in sources/Makefile.am.
# Note that as this is all generated code, we disable various warnings.

service_cppflags = \
	-DG_LOG_DOMAIN=\"Prestwood\" \
        $(AM_CPPFLAGS)
service_cflags = \
        $(GLIB_CFLAGS) \
        $(WARN_CFLAGS) \
        $(CODE_COVERAGE_CFLAGS) \
        $(AM_CFLAGS)
service_libadd = \
        $(GLIB_LIBS) \
        $(CODE_COVERAGE_LIBS) \
        $(AM_LIBADD)
service_ldflags = \
        $(WARN_LDFLAGS) \
        $(AM_LDFLAGS)
service_codegen_flags = \
        --generate-docbook $(top_builddir)/docs/dbus \
        --c-namespace=Prestwood \
        --interface-prefix=org.apertis.Prestwood \
        $(NULL)

# Generic rules
%.c: %.xml Makefile
	$(AM_V_GEN)$(GDBUS_CODEGEN) \
                $(service_codegen_flags) --generate-c-code $* $<

%.h: %.c %.xml
	@: # generated as a side-effect of the .c

docs-%.xml: %.c %.xml
	@: # generated as a side-effect of the .c

prestwood_sources_h = $(prestwood_sources_xml:.xml=.h)
prestwood_sources_c = $(prestwood_sources_xml:.xml=.c)
prestwood_built_docs = $(addprefix docs-,$(prestwood_sources_xml))

EXTRA_DIST = $(prestwood_sources_xml)

BUILT_SOURCES = \
	$(prestwood_sources_h) \
	$(prestwood_sources_c) \
	$(prestwood_built_docs) \
	$(NULL)

CLEANFILES = $(BUILT_SOURCES)

includesfidir = $(includedir)/prestwood
nodist_includesfi_HEADERS = $(prestwood_sources_h)

# prestwood-app-db-handler
prestwood_sources_xml = org.apertis.Prestwood.Service.xml

lib_LTLIBRARIES = libprestwoodiface.la

nodist_libprestwoodiface_la_SOURCES = \
        $(prestwood_sources_xml:.xml=.c) \
        $(prestwood_sources_xml:.xml=.h) \
        $(NULL)

libprestwoodiface_la_CPPFLAGS = $(service_cppflags)
libprestwoodiface_la_CFLAGS = $(service_cflags)
libprestwoodiface_la_LIBADD = $(service_libadd)
libprestwoodiface_la_LDFLAGS = $(service_ldflags)

-include $(INTROSPECTION_MAKEFILE)
INTROSPECTION_GIRS =
INTROSPECTION_SCANNER_ARGS = \
	$(WARN_SCANNERFLAGS) \
	$(NULL)

if HAVE_INTROSPECTION

PrestwoodInterface-@PRESTWOOD_API_VERSION@.gir: libprestwoodiface.la
PrestwoodInterface_@PRESTWOOD_API_VERSION@_gir_FILES = \
	$(nodist_libprestwoodiface_la_SOURCES) \
	$(NULL)
PrestwoodInterface_@PRESTWOOD_API_VERSION@_gir_NAMESPACE = PrestwoodInterface
PrestwoodInterface_@PRESTWOOD_API_VERSION@_gir_VERSION = @PRESTWOOD_API_VERSION@
PrestwoodInterface_@PRESTWOOD_API_VERSION@_gir_LIBS = libprestwoodiface.la
PrestwoodInterface_@PRESTWOOD_API_VERSION@_gir_CFLAGS = $(AM_CPPFLAGS) $(CPPFLAGS)
PrestwoodInterface_@PRESTWOOD_API_VERSION@_gir_SCANNERFLAGS = \
	--identifier-prefix=Prestwood \
	--symbol-prefix=prestwood \
	$(WARN_SCANNERFLAGS) \
	$(NULL)
PrestwoodInterface_@PRESTWOOD_API_VERSION@_gir_INCLUDES = \
	GLib-2.0 \
	Gio-2.0 \
	$(NULL)

INTROSPECTION_GIRS += PrestwoodInterface-@PRESTWOOD_API_VERSION@.gir

girdir = $(datadir)/gir-1.0
gir_DATA = $(INTROSPECTION_GIRS)

typelibdir = $(libdir)/girepository-1.0/
typelib_DATA = $(INTROSPECTION_GIRS:.gir=.typelib)

CLEANFILES += $(gir_DATA) $(typelib_DATA)
endif

-include $(top_srcdir)/git.mk
