/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "prestwood-internal.h"
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

gint main (gint argc, gchar **argv)
{

	GMainLoop *mainloop = g_main_loop_new (NULL, TRUE);
	if (mainloop == NULL)
		return -1;

	if (prestwood_init () != 0)
		return -1;

	mainloop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (mainloop);
	g_bus_unown_name(prestwood_get_default()->owner_id);


	return 0;
}

